// import Layout from '../components/MyLayout.js'

// export default (props) => (
//     <Layout>
//        <h1>{props.url.query.title}</h1>
//        <p>This is the blog post content.</p>
//     </Layout>
// )

import Layout from '../components/MyLayout.js'
import fetch from 'isomorphic-unfetch'

const Post = (props) => (
    <Layout>
        <h1>{props.show.name}</h1>
        <p>{props.show.summary.replace(/<[/]?p>/g, '')}</p>
        <img src={props.show.image.medium} />
        <style jsx>{`
      h1, a {
        font-family: "Arial";
      }

      ul {
        padding: 0;
      }

      li {
        list-style: none;
        margin: 5px 0;
      }

      a {
        text-decoration: none;
        color: black;
      }

      a:hover {
        opacity: 0.6;
      }
    `}</style>
    </Layout>
)

Post.getInitialProps = async function (context) {
    const { id } = context.query
    const res = await fetch(`https://api.tvmaze.com/shows/${id}`)
    const show = await res.json()

    console.log(`Fetched show: ${JSON.stringify(show)}`)

    return { show }
}

export default Post